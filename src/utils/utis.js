export const client = {
    'country': 'eu'
}

export const server = {
    'secretKey': 'sk_a1b305512a52fdc81307bddc'
}

export function Utils () {
    // Adapters
    var adapters = {
      'isConfigValid': function () {
        return isConfigValid()
      }
    }
  
    /**
     * Is the config valid
     * @return {Boolean} Boolean Config is valid.
     * @public
     */
    var isConfigValid = function () {
      if (!server) {
        console.error('Unable to find server object!')
        return false
      }
  
      if (!client) {
        console.error('Unable to find client object!')
        return false
      }
  
      return true
    }
  
    // Return adapters (must be at end of adapter)
    return adapters
  }