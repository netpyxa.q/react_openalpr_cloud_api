import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import './App.css';
import { server, client, Utils } from '../utils/utis'
import { OpenALPRCloudApi } from './openAlpr'
import RecipeReviewCard from '../components/Carousel/Carousel'
import CircularProgress from '@material-ui/core/CircularProgress';
import _ from 'lodash'
class App extends Component {
  state = {
    file: null,
    apiKey: null,
    client: null,
    message: null,
    car: null,
    carsLocal: [],
    loading: false
  }

  handleInput = (e) => {
    this.setState({
      file: e.target.value
    })
    this.runOpenALPR(e)
  }

  handleImage = (data) => {
    let i = 0
    this.setState({
      image: data,
      loading: true
    })
  }

  componentDidMount() {
    this.setState({
      apiKey: server,
      client: client,
      carsLocal: JSON.parse(localStorage.getItem('cars')) || []
    })
  }
  
  runOpenALPR = (input) => {
    this.showImage(input)
  }

  showImage = (input) => {
    if (!window.FileReader) {
      alert('Browser does not support FileReader!')
      return
    }

    if (input.currentTarget.files && input.currentTarget.files[0]) {
      var reader = new FileReader()
      const {cloudAPISuccess, cloudAPIError, handleImage} = this
      // this.setState({
      //   image: reader.e.target.result
      // })
      reader.onload = function (e) {
        var imageDataURL = e.target.result
        // $('#uploadedImage').attr('src', imageDataURL)
        handleImage(imageDataURL)
        OpenALPRCloudApi().retrievePlate(imageDataURL)
          .then(function (response) {
            cloudAPISuccess(response)
          })
          .catch(function (err) {
            cloudAPIError(err)
          })
      }
      reader.readAsDataURL(input.currentTarget.files[0])
    }
  }

  cloudAPIError = (err) => {
    if (err && err.responseText) {
      alert('Failed to retrieve data ' + err.responseText)
    } else {
      alert('Exception occured' + err)
    }
  }

  cloudAPISuccess = (response) => {
    if (!response.results || response.results.length === 0) {
      alert('No plates found!')
      this.setState({
        loading: false
      })
      return
    }
    for (var key in response.results) {
      var result = response.results[key]
      this.displayData(result)
    }
    this.setState({
      loading: false
    })
    var creditsRemaining = response.credits_monthly_total - response.credits_monthly_used
  }

  displayData = (message) => {
    console.log(message)
    this.setState({
      car: {
        image: this.state.image,
        plate: message.plate,
        region: message.region,
        make: _.get(message, 'vehicle.make[0].name', ''),
        model: _.get(message, 'vehicle.make_model[0].name', ''),
        type: _.get(message, 'vehicle.body_type[0].name', ''),
        color: _.get(message, 'vehicle.color[0].name', ''),
        year: _.get(message, 'vehicle.year[0].name', ''),
        processing_time: _.get(message, 'processing_time_ms', ''),
      }
    })

    let swap = JSON.parse(localStorage.getItem('cars')) || []
    let buffer = swap.slice(0, 10)
    localStorage.setItem("cars", JSON.stringify([this.state.car, ...buffer]))
    this.setState({
      carsLocal: [this.state.car, ...buffer]
    })
  }


  render() {
    return(
      <React.Fragment>
      <section className='container actions'>
        <div className='box'>
        <Button
          variant="contained"
          component="label"
        >
          Завантажити зображення
          <input
            type="file"
            onChange={
              (e) => {
                this.handleInput(e)
              }
            }
            style={{ display: "none" }}
          />
        </Button>
        </div>
      </section>
      <section className='container big'>
        <div className='box loading'>
          {this.state.loading ? <CircularProgress /> : this.state.car ? <RecipeReviewCard item={this.state.car} /> : null}
        </div>
      </section>
      <section className='container'>
        <div className='small'>
          {this.state.carsLocal.map((item, i) => {
            return <RecipeReviewCard key={i + Date.now()} item={item} />
          })}
        </div>
      </section>
      </React.Fragment>
    )
  }
}

export default App;
