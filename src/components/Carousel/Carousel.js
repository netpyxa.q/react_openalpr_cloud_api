import React from 'react';

class RecipeReviewCard extends React.Component{

render() {  
        return (
            <div className='card'>
                <div className='card-image'>
                    <span className='percent'>{Math.floor(this.props.item.processing_time * 100) / 100} мс</span>
                    <img src={this.props.item.image} />
                </div>
                <div className='card-content'>
                    <p>Номер автомобіля: {this.props.item.plate}</p>
                    <p>Регіон: {this.props.item.region}</p>
                    <p>Марка: {this.props.item.make}</p>
                    <p>Модель: {this.props.item.model}</p>
                    <p>Тип автомобіля: {this.props.item.type}</p>
                    <p>Колір автомобіля: {this.props.item.color}</p>
                    <p>Рік: {this.props.item.year}</p>
                </div>
            </div>
        );
    }
}

export default RecipeReviewCard;